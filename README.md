# hipsycl-testing

Infrastructure for regularly testing versions of ROCm, hipSYCL, and GROMACS against each other. The aim is to uncover problems quickly and stablize GROMACS on hipSYCL on ROCm sooner.